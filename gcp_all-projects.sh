#!/bin/bash

INIT_PROJECT="$(gcloud info --format='table[no-heading](config.project)')"

echo "[*] gcp_enum'ing all projects"
for i in $(gcloud projects list --quiet | awk '{print $1}' | tail -n +2); do
    echo "[+] Enumerating $i"
    gcloud config set project $i
    ./gcp_enum.sh
done

# be nice and set back to initial project 
gcloud config set project $INIT_PROJECT
